<?php
namespace App\Http\Controllers\Request;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Req;

class RequestController extends Controller{
	public $_request;
	public function __construct(Request $request){
		$this->_request = $request;
	}

	public function action(){
		$data = ['success'=>false];
		$params = $this->_request->input('params');

		$req = Req::add($params);
		if($req)
			$data = ['success'=>true];

		return response()->json($data);
	}
}