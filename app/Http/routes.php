<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
*/

$app->get('/', function () use ($app) {
    return $app->welcome();
});

// Site
$app->get('site/{name}','Site\SiteController@index');

//Menu
$app->post('menu','Menu\MenuController@listAll');

//Project
$app->post('project','Project\ProjectController@listAll');
$app->post('project-advance','Project\ProjectController@listAllAdvance');
$app->post('project-strata','Project\ProjectController@listStrata');
$app->post('project/{slug}','Project\ProjectController@detail');

//ProjectArticle
$app->post('project-article','ProjectArticle\ProjectArticleController@listAll');
$app->post('project-article-advance','ProjectArticle\ProjectArticleController@listAllAdvance');

//Category
$app->post('category','Category\CategoryController@listAll');
$app->post('category-advance','Category\CategoryController@listAllAdvance');

//Gallery
$app->post('gallery-advance','Gallery\GalleryController@listAll');
$app->post('gallery/row','Gallery\GalleryController@getByArticle');
$app->get('gallery/{id}','Gallery\GalleryController@detail');

//Contact
$app->post('contact/create','Contact\ContactController@create');

//Request
$app->post('request','Request\RequestController@action');

//Search
$app->post('search/article','Search\SearchController@articleOnly');

// Article
$app->post('article','Article\ArticleController@listAll');
$app->post('article-all','Article\ArticleController@getAll');
$app->post('article-advance','Article\ArticleController@listAllAdvance');
$app->post('article/row','Article\ArticleController@getRow');
$app->get('article/download/{id}','Article\ArticleController@download');
$app->post('{slug}','Article\ArticleController@detail');