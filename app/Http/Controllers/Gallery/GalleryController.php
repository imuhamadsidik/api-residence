<?php
namespace App\Http\Controllers\Gallery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Gallery;

class GalleryController extends Controller{
	public $_request;

	public function __construct(Request $request){
		$this->_request = $request;
	}

	public function listAll(){
		$data=[];
		$params = $this->_request->input('params');
		$paramAdvance = $this->_request->input('advance')?$this->_request->input('advance'):[];
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');
		$paginate = $this->_request->input('paginate');

		$album = Gallery::getJoinRow($params,$order,$asc);
		foreach($paramAdvance as $key=>$value){
			foreach($value as $vKey=>$vValue){
				if($key=='is null'){
					$album = $album->whereNull($vValue);
				}
				elseif($key=='or'){
					$album = $album->orWhere($vKey,$vValue);
				}
				else{
					$album = $album->where($vKey,$key,$vValue);
				}
			}

		}
		if($paginate){
			$album = $album->paginate($paginate);
			foreach ($album->items() as $ix=>$p) {
				$album->items()[$ix]->cover = "";
				$dir = PUBLIC_IMAGE_PATH.'gallery/'.$p->path;
				$scan = scandir($dir);
			
				$scan = array_diff( $scan,['.','..']);
				$fileCover = $scan[2];
				if(is_dir($dir.'/'.$fileCover))
					$fileCover = $scan[3];
				
				$album->items()[$ix]->cover = $p->path.'/'.$fileCover;					
			}
		}
		else{
			$album = $album->get();
			foreach ($album as $ix=>$p) {
				$album[$ix]->cover = "";
				$dir = PUBLIC_IMAGE_PATH.'gallery/'.$p->path;
				$scan = scandir($dir);
				$scan = array_diff($scan,['.','..']);
				$fileCover = $scan[2];
				if(is_dir($dir.'/'.$fileCover))
					$fileCover = $scan[3];

				$album[$ix]->cover = $p->path.'/'.$fileCover;	
			}
		}
			
		if($album)
			$data = $album;

		return response()->json($data);
	}
	public function detail($id){

		$data = [];
		$gallery = Gallery::getRow(['article_id'=>$id])->first();
		$dataFile = [];
		if($gallery){
			$dir = $gallery->path;
			$directory = PUBLIC_IMAGE_PATH .'gallery/'.$dir;
			$scan = scandir($directory);
			$no = 0;
			
			for ($i = 2; $i < count($scan); $i++) {
				if(!is_dir($directory.'/'.$scan[$i])){
					$exp = explode('_', $scan[$i]);

					$dataFile[] = IMAGE_URL.'gallery/'.$dir.'/'.$scan[$i];			
				
					$no++;
				}
			}
			$data = $dataFile;
		}
		return response()->json($data);	
	}	
	public function getByArticle(){
		$data=[];
		$params = $this->_request->input('params');
		$paramAdvance = $this->_request->input('advance')?$this->_request->input('advance'):[];
		
		$album = Gallery::getJoinRow($params);
		foreach($paramAdvance as $key=>$value){
			foreach($value as $vKey=>$vValue){
				if($key=='is null'){
					$album = $album->whereNull($vValue);
				}
				elseif($key=='or'){
					$album = $album->orWhere($vKey,$vValue);
				}
				else{
					$album = $album->where($vKey,$key,$vValue);
				}
			}

		}
		$album = $album->first();
		if($album){
			$dir = $album->path;
			$directory = PUBLIC_IMAGE_PATH .'gallery/'.$dir;
			$scan = scandir($directory);
			$no = 0;
			
			for ($i = 2; $i < count($scan); $i++) {
				if(!is_dir($directory.'/'.$scan[$i])){
					$exp = explode('_', $scan[$i]);

					$dataFile[] = IMAGE_URL.'gallery/'.$dir.'/'.$scan[$i];			
				
					$no++;
				}
			}
			$data = $dataFile;
		}

		return response()->json($data);
	}
}