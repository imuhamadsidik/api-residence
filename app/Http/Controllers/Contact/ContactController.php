<?php
namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Contact;

class ContactController extends Controller{
	public $_request;
	public function __construct(Request $request){
		$this->_request = $request;
	}

	public function create(){
		$param = $this->_request->input('params');
		$insert = Contact::add($param);

		$data=['status'=>'false'];
		if($insert)
			$data = ['status'=>'true'];

		return response()->json($data);

	}
}