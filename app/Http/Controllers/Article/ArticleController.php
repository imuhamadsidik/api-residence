<?php
namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Article;

class ArticleController extends Controller{
	public $_request;

	public function __construct(Request $request){
		$this->_request = $request;
	}

	public function listAll(){
		$data=[];
		$params = $this->_request->input('params');
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');
		$paginate = $this->_request->input('paginate');

		$article = Article::getJoinRow($params,$order,$asc);
		if($paginate)
			$article = $article->paginate($paginate);
		else
			$article = $article->get();
		
		if($article)
			$data = $article;

		return response()->json($article);
	}

	public function getAll(){
		$data=[];
		$params = $this->_request->input('params');
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');

		$article = Article::getJoinRow($params,$order,$asc);
			$article = $article->get();
		
		if($article)
			$data = $article;

		return response()->json($article);
	}


	public function detail($slug){
		$data = [];
		$params = $this->_request->input('params');
		if(is_array($params))
			$params['slug'] = $slug;
		else
			$params['slug'] = $slug;

		
		$article = Article::getRow($params)->first();
		if($article){
			$data = $article;
			if(is_null($data->clicked)||($data->clicked==0))
				Article::change(['clicked'=>1],$data->article_id);
			else{
				$clicked = $data->clicked + 1;
				Article::change(['clicked'=>$clicked],$data->article_id);
			}
		}
			

		return response()->json($data);
	}

	public function listAllAdvance(){
		$data=[];
		$params = $this->_request->input('params');
		$paramAdvance = $this->_request->input('advance')?$this->_request->input('advance'):[];
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');
		$paginate = $this->_request->input('paginate');

		$article = Article::getJoinRow($params,$order,$asc);

		foreach($paramAdvance as $key=>$value){
			foreach($value as $vKey=>$vValue){
				if($key=='is null'){
					$article = $article->whereNull($vValue);
				}
				else{
					$article = $article->where($vKey,$key,$vValue);
				}
			}

		}

		if($paginate)
			$article = $article->paginate($paginate);
		else
			$article = $article->get();
	
		if($article)
			$data = $article;

		return response()->json($data);
	}

	public function getRow(){
		$data=[];
		$params = $this->_request->input('params');
		$paramAdvance = $this->_request->input('advance')?$this->_request->input('advance'):[];
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');
		
		$article = Article::getJoinRow($params,$order,$asc);

		foreach($paramAdvance as $key=>$value){
			foreach($value as $vKey=>$vValue){
				if($key=='is null'){
					$article = $article->whereNull($vValue);
				}
				else{
					$article = $article->where($vKey,$key,$vValue);
				}
			}

		}

		$article = $article->first();
	
		if($article)
			$data = $article;

		return response()->json($data);
	}

	public function download($id){
		$post = Article::getRow(['article_id'=>$id])->first();
		if($post){

			// $file = 'D:\apache\htdocs\sb_\public\files/'.$post->files;
			$file = 'E:\Apache24\htdocs\sb_\public\files/'.$post->files;
			if (file_exists($file)) {
			    header('Content-Description: File Transfer');
			    header('Content-Type: application/pdf');
			    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    	header('Expires: 0');
		    	header('Cache-Control: must-revalidate');
		    	header('Pragma: public');
		    	header('Content-Length: ' . filesize($file));
		    	readfile($file);
		    	
		    }
		}
	}
}