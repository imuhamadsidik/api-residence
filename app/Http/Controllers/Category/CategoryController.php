<?php
namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Category;

class CategoryController extends Controller{
	public $_request;

	public function __construct(Request $request){
		$this->_request = $request;
	}

	public function listAll(){
		$params = $this->_request->input('params');
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');
		$paginate = $this->_request->input('paginate');

		$data=[];
		$category = Category::getJoinRow($params,$order);
		if($paginate)
			$category = $category->paginate($paginate);
		else
			$category = $category->get();

		if($category)
			$data = $category;

		return response()->json($data);
	}

	public function listAllAdvance(){
		$data = [];

		$params = $this->_request->input('params');
		$order = $this->_request->input('order');
		$paramAdvance = $this->_request->input('advance')?$this->_request->input('advance'):[];
		$asc = $this->_request->input('asc');
		$paginate = $this->_request->input('paginate');

		$category = Category::getJoinRow($params,$order,$asc);

		foreach($paramAdvance as $key=>$value){
			foreach($value as $vKey=>$vValue){
				if($key=='is null'){
					$category = $category->whereNull($vValue);
				}
				else{
					$category = $category->where($vKey,$key,$vValue);
				}
			}

		}

		if($paginate)
			$category = $category->paginate($paginate);
		else
			$category = $category->get();
	
		if($category)
			$data = $category;

		return response()->json($data);
	}
}