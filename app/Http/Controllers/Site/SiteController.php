<?php
namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Site;

class SiteController extends Controller{
	public $_request;

	public function __construct(Request $request){
		$this->_request = $request;
	}
	
	/**
     * Update the specified user.
     *
     * @param  string  $name
     * @return Response
     */

	public function index($name){
		$data = [];
		$site = Site::getRow(['name'=>$name])->first();
		if($site)
			$data = $site;

		return response()->json($data);
	}
}