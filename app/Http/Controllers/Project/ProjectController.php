<?php
namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;

class ProjectController extends Controller{
	public $_request;

	public function __construct(Request $request){
		$this->_request = $request;
	}

	public function listAll(){
		$data=[];
		$params = $this->_request->input('params');
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');
		$paginate = $this->_request->input('paginate');

		$project = Project::getJoinRow($params,$order,$asc);

		if($paginate)
			$project = $project->paginate($paginate);
		else
			$project = $project->get();

		if($project)
			$data = $project;

		return response()->json($project);
	}
	public function detail($slug){
		$data = [];
		$params = $this->_request->input('params');
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');
		if(is_array($params))
			$params['slug'] = $slug;
		else
			$params['slug'] = $slug;

		
		$project = Project::getRow($params,$order,$asc)->first();
		if($project){
			$data = $project;
			if(is_null($data->clicked)||($data->clicked==0))
				Project::change(['clicked'=>1],$data->project_id);
			else{
				$clicked = $data->clicked + 1;
				Project::change(['clicked'=>$clicked],$data->project_id);
			}
		}

		return response()->json($data);
	}

	public function listAllAdvance(){
		$data=[];
		$params = $this->_request->input('params');
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');
		$paramAdvance = $this->_request->input('advance')?$this->_request->input('advance'):[];
		$paginate = $this->_request->input('paginate');

		$project = Project::getJoinRow($params,$order,$asc);

		foreach($paramAdvance as $key=>$value){
			foreach($value as $vKey=>$vValue){
				if($key=='is null'){
					$project = $project->whereNull($vValue);
				}
				else{
					$project = $project->where($vKey,$key,$vValue);
				}
			}

		}
		if($paginate)
			$project = $project->paginate($paginate);
		else
			$project = $project->get();
	
		if($project)
			$data = $project;

		return response()->json($data);
	}
	public function listStrata(){
		$data=[];
		$params = $this->_request->input('params');
		$order = $this->_request->input('order');
		$asc = $this->_request->input('asc');
		$paginate = $this->_request->input('paginate');

		$project = Project::getJoinRow($params,$order,$asc)
				   ->whereIn('a.slug', ['rainbow-springs-condovillas','serpong-m-town']);

		if($paginate)
			$project = $project->paginate($paginate);
		else
			$project = $project->get();

		if($project)
			$data = $project;

		return response()->json($project);
	}
}