<?php

namespace App\Http\Middleware;

use Closure;

class ConstantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        // define('PUBLIC_IMAGE_PATH','D:\apache\htdocs\sb_\public\images/');
        define('PUBLIC_IMAGE_PATH','E:\apache24\htdocs\sb_\public\images/');
        // define('PUBLIC_URL','http://images-residence.summarecon.com/');
        define('PUBLIC_URL','http://www.summareconbekasi.com/public/');
        define('IMAGE_URL',PUBLIC_URL.'images/');
        define('GALLERY_URL',PUBLIC_URL.'gallery/');
        return $next($request);
    }
}
