<?php
namespace App\Http\Controllers\Menu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller{
	public $_request;

	public function __construct(Request $request){
		$this->_request = $request;
	}
	
	/**
     * Update the specified user.
     *
     * @return Response
     */

	public function listAll(){
		$data = [];
		$params = ($this->_request->input('params'));

		$menu = Menu::getJoinRow($params,'a.position')->get();
		if($menu)
			$data['menus'] = $menu;

		return response()->json($data);
	}
}
