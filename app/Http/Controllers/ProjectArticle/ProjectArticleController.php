<?php
namespace App\Http\Controllers\ProjectArticle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ProjectArticle;

class ProjectArticleController extends Controller{
	public $_request;

	public function __construct(Request $request){
		$this->_request = $request;
	}

	public function listAll(){
		$data = [];
		$params = $this->_request->input('params');
		$order = $this->_request->input('order');

		$projectArticle = ProjectArticle::getJoinRow($params,$order)->get();
		if($projectArticle)
			$data = $projectArticle;

		return response()->json($data);
	}

	public function listAllAdvance(){
		$data=[];
		$params = $this->_request->input('params');
		$paramAdvance = $this->_request->input('advance');
		$order = $this->_request->input('order');
		//return response()->json($paramAdvance);

		$projectArticle = ProjectArticle::getJoinRow($params,$order);

		foreach($paramAdvance as $key=>$value){
			foreach($value as $vKey=>$vValue){
				if($key=='is null'){
					$projectArticle = $projectArticle->whereNull($vValue);
				}
				else{
					$projectArticle = $projectArticle->where($vKey,$key,$vValue);
				}
			}

		}
		$projectArticle = $projectArticle->get();
		if($projectArticle)
			$data = $projectArticle;

		return response()->json($data);
	}
}