<?php
namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ProjectArticle;
use App\Models\Project;
use App\Models\Article;

class SearchController extends Controller{
	public $_request;

	public function __construct(Request $request){
		$this->_request = $request;
	}

	public function articleOnly(){
		$data=[];
		$params = $this->_request->input('params');
		$paramAdvance = $this->_request->input('advance');
		$order = $this->_request->input('order');

		$article = Article::getJoinRow($params,$order);

		foreach($paramAdvance as $key=>$value){
			foreach($value as $vKey=>$vValue){
				if($key=='is null'){
					$article = $article->whereNull($vValue);
				}
				else{
					$article = $article->where($vKey,$key,$vValue);
				}
			}

		}
		$article = $article->get();
		if($article){
			foreach ($article as $art) {
				$projectArticle = ProjectArticle::getRow(['article_id'=>$art->article_id])->first();
				if(!$projectArticle){
					$data[] = $art;
				}
			}
		}
		return response()->json($data);
	}
}